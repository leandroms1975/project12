<?php
  
  
  session_start();

  /*
  echo '<pre>';
  print_r($_SESSION);
  echo '</pre>';

  //remover índices do array de sessão
  //unset()

  unset($_SESSION['x']); //inteligência para executar o índice apenas se existir

  echo '<pre>';
  print_r($_SESSION);
  echo '</pre>';

  //destruir a variável de sessão
  //session_destroy()

  session_destroy(); //a sessão será destruída mas ainda será exibida
  //há necessidade de forçar o redirecionamento para a limpeza da sessão.

  echo '<pre>';
  print_r($_SESSION);
  echo '</pre>';

  */

  //forma correta de se destruir uma sessão
  session_destroy();
  header('Location: index.php'); //redirecionamento para limpeza das sessões

  
?>
